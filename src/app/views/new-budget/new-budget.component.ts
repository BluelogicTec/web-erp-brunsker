import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxDatatableService } from 'src/app/shared/services/datatable-service.service';

@Component({
  selector: 'app-new-budget',
  templateUrl: './new-budget.component.html',
  styleUrls: ['./new-budget.component.scss']
})
export class NewBudgetComponent implements OnInit {
  
  private oneDay = 24 * 3600 * 1000;
  private now!: Date;
  private value!: number ;
  private data!: any[];
  private timer: any;
  public rows: any;
  public rowsHistory: any;
  private columns: any;
  public tabIndex = 0;

  start: any;
  form: any;
  myData!: any[];
  selected = [];
  closeResult = '';

  stepper: number = 1;

  credit = [
    {credit: 1, name: 'liquidado'},
    {credit: 2, name: 'em aberto'},
    {credit: 3, name: 'cancelado'},
  ]

  typeCreditText = [
    {name: 'Crédito liquidado', credit: 1},
    {name: 'Crédito em aberto', credit: 2},
    {name: 'Crédito candelado', credit: 3},
  ]

  totalCount: Number = 0;
  rowsHistorytotalCount: Number = 0;
  dataParams: any = {
    page_num: '',
    page_size: ''
  };
  currentDate = new Date();

  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  tarFormGroup: FormGroup;

  rowsOrc: any;
  rows_item: any;
  loading = false

  constructor(
    private ngxDatatableService: NgxDatatableService,
    private localeService: BsLocaleService,
    private _formBuilder: FormBuilder,
    private modalService: NgbModal,

  ) { 
    this.form = new FormGroup({
      dateYMD: new FormControl(new Date()),
      dateFull: new FormControl(new Date()),
      dateMDY: new FormControl(new Date()),
      // dateRange: new FormControl()
    });

    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.tarFormGroup = this._formBuilder.group({
      tarCtrl: ['', Validators.required]
    });

  }
  // formGroup = new FormGroup({ secondCtrl: new FormControl(''), })

  ngOnInit(): void {
    this.rows = this.ngxDatatableService.steep_item; 
    this.rows_item = this.ngxDatatableService.steep_item_remove; 

    this.rowsHistory = this.ngxDatatableService.historyClient;

    this.rowsOrc = this.ngxDatatableService.creditConsultationOrc;

    setTimeout(() =>{
      this.loading = true
    },3000);


  }

  onSelect(row: any) {
    console.log(row)
  }

  nextStepper (setStepper: number) {
    // alert(setStepper)

    const tabCount = 3;
    this.stepper = setStepper
    this.tabIndex = (this.tabIndex + 1) % tabCount;
  }
  previousStepper (setStepper: number) {
    // alert(setStepper)

    const tabCount = 3;
    this.stepper = setStepper
    this.tabIndex = (this.tabIndex - 1) % tabCount;
  }

  addItem() {
    alert('add')
  }
  
  addItemTable(item: any) {

    console.log(item)
    const qtde = ++item.qtde;
    this.rows.qtde = qtde;
  }
  removeItemTable(item: any) {

    console.log(item)
    if(item.qtde > 0) {
      const qtde = --item.qtde;
      this.rows.qtde = qtde;
    }
  }
  

  
  openSales(content: any) {
    this.modalService.open(content, { windowClass: 'new-budget'}).result.then((result) => {
      console.log(result)
      if(result === 'ok'){
      }
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  
  open(content: any) {
    this.modalService.open(content, { windowClass: 'sidebar'}).result.then((result) => {
      console.log(result)
      if(result === 'ok'){
      }
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  
  secondSetForm () {
    this.secondFormGroup.setValue({
      secondCtrl: 'ok'
    })
  } 
}

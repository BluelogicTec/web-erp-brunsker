import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxDatatableService } from 'src/app/shared/services/datatable-service.service';

@Component({
  selector: 'app-customer-credit',
  templateUrl: './customer-credit.component.html',
  styleUrls: ['./customer-credit.component.scss']
})
export class CustomerCreditComponent implements OnInit {

  private oneDay = 24 * 3600 * 1000;
  private now!: Date;
  private value!: number ;
  private data!: any[];
  private timer: any;
  public rows: any;
  public rowsHistory: any;
  private columns: any;
  start: any;
  form: any;
  myData!: any[];
  selected = [];

  credit = [
    {credit: 1, name: 'liquidado'},
    {credit: 2, name: 'em aberto'},
    {credit: 3, name: 'cancelado'},
  ]

  typeCreditText = [
    {name: 'Crédito liquidado', credit: 1},
    {name: 'Crédito em aberto', credit: 2},
    {name: 'Crédito candelado', credit: 3},
  ]

  totalCount: Number = 0;
  rowsHistorytotalCount: Number = 0;
  dataParams: any = {
    page_num: '',
    page_size: ''
  };
  currentDate = new Date();

  constructor(
    private ngxDatatableService: NgxDatatableService,
    private localeService: BsLocaleService
  ) {
    localeService.use('pt-br');
    this.form = new FormGroup({
      dateYMD: new FormControl(new Date()),
      dateFull: new FormControl(new Date()),
      dateMDY: new FormControl(new Date()),
      // dateRange: new FormControl()
    });
  }

  ngOnInit(): void {
    this.rows = this.ngxDatatableService.listCredit;
    this.totalCount = this.ngxDatatableService.listCredit_count;

    this.rowsHistory = this.ngxDatatableService.historyClient;
  }

  onSelect(row: any) {
    console.log(row)
  }

}

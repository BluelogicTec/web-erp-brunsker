import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerCreditComponent } from './customer-credit.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Routes, RouterModule } from '@angular/router';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxEchartsModule } from 'ngx-echarts';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { InlineSVGModule } from 'ng-inline-svg';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

const routes: Routes = [
  {
    path: '',
    component: CustomerCreditComponent,
  },
];

@NgModule({
  declarations: [CustomerCreditComponent],
  imports: [
    NgbModule,
    CommonModule,
    RouterModule.forChild(routes),
    NgxEchartsModule,
    NgxDatatableModule,
    AngularSvgIconModule,
    HttpClientModule,
    InlineSVGModule,
    TooltipModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule
  ]
})
export class CustomerCreditModule { }

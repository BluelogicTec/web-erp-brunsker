import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxDatatableService } from 'src/app/shared/services/datatable-service.service';
// import { echartStyles } from 'src/app/shared/echart-styles';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  options: any;
  updateOptions: any;

  private oneDay = 24 * 3600 * 1000;
  private now!: Date;
  private value!: number ;
  private data!: any[];
  private timer: any;
  public rows: any;
  private columns: any;
  start: any;
  form: any;
  bsValue: any = '03/10/2021 - 03/19/2021';
  myData!: any[];

  totalCount: Number = 0;
  dataParams: any = {
    page_num: '',
    page_size: ''
  };
  currentDate = new Date();
  constructor(
    private ngxDatatableService: NgxDatatableService,
    private localeService: BsLocaleService
  ) {
    localeService.use('pt-br');
    this.form = new FormGroup({
      dateYMD: new FormControl(new Date()),
      dateFull: new FormControl(new Date()),
      dateMDY: new FormControl(new Date()),
      dateRange: new FormControl([
        new Date(),
        new Date(this.currentDate.setDate(this.currentDate.getDate() + 7))
      ])
    });
  }

  ngOnDestroy() {
    clearInterval(this.timer);
  }
  getAllHeroList() {
    this.rows = this.ngxDatatableService.hero_pages;
    this.totalCount = this.ngxDatatableService.total_count;
  }


  ngOnInit() {
    this.form = new FormGroup({
      dateYMD: new FormControl(new Date()),
      dateFull: new FormControl(new Date()),
      dateMDY: new FormControl(new Date()),
      dateRange: new FormControl([
        new Date(),
        new Date(this.currentDate.setDate(this.currentDate.getDate() + 7))
      ])
    });
    this.dataParams.page_num = 1;
    this.dataParams.page_size = 20;
    this.getAllHeroList();
    // this.rows = [
    //   { name: 'Austin', gender: 'Male', company: 'Swimlane' },
    //   { name: 'Dany', gender: 'Male', company: 'KFC' },
    //   { name: 'Molly', gender: 'Female', company: 'Burger King' }
    // ];
    // this.columns = [{ prop: 'name' }, { name: 'Gender' }, { name: 'Company' }];

    this.data = [0, 20, 150, 934, 1290, 1330, 1320, 0, 950, 1320, 0, 932, 901, 934, 1290, 3000, 1320, 1320, 1320, 1320, 820, 932, 901, 934, 1290, 1330, 1320, 1320, 1320, 522, 558 ]
   const myDateFormatter = this.data.map(item => {

      var formatter = new Intl.NumberFormat('pt-br', {
        style: 'currency',
        currency: 'BRL',
      });
      formatter.format(item);
      //  item = item.toLocaleString('pt-BR')
      return item
      // this.myData = ;
    })
    this.options = {
      color: ['#3398DB'],

      tooltip: {
        color: ['#3398DB'],
        trigger: 'axis',
        formatter: (params: any) => {
          params = params[0];
          params.data = parseFloat(params.data);
          return params.data.toLocaleString('pt-BR', {style: 'currency', currency: 'BRL'});
        },
        label: {
          color: '#ff',
       } ,
        axisPointer: {
          type: 'shadow',
          animation: false,
          color: ['#3398DB'],

        },
      },
      grid: {
        left: '0%',
        right: '0%',
        bottom: '3%',
        containLabel: true
    },
      xAxis: {
        type: 'category',
        boundaryGap: false,
        axisLabel: {
          color: '#00644E'
        },
        data: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10','11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '12', '22', '23', '24', '25', '26', '27', '28', '29', '30' ,'31'],

      },
      yAxis: {
        type: 'value',
        axisLabel: {
          formatter: ' R$ {value}',
          color: '#00644E',
          // backgroundColor: '#F7F7F7',
          // borderColor: '#999999',
          // height: 51,
          // width: 50
        },
        backgroundColor: 'red',
      },
      series: [
        {
          data: myDateFormatter,
          type: 'line',
          markPoint: {
            symbolSize: 1,
            symbolOffset: [0, '50%'],
            label: {
                formatter: '{{c|{c}}',
                backgroundColor: '#2C2C2C',
                borderColor: '#2C2C2C',
                borderWidth: 1,
                borderRadius: 4,
                padding: [2, 5],
                lineHeight: 26,
                position: 'top',
                distance: 5,
                rich: {
                    c: {
                        color: '#fff',
                        textBorderColor: '#000',
                        textBorderWidth: 1,
                        fontSize: 14
                    }
                }
            },
            data: [
                {type: 'max', name: ''},
            ]
        }

        //   markPoint: {
        //     data: [
        //         {type: 'max'},
        //     ]
        // },
      //   markPoint : {
      //     data: [
      //     {type: 'max'}
      //     ],
      //     symbolSize: 5,       // 标注大小，半宽（半径）参数，当图形为方向或菱形则总宽度为symbolSize * 2
      //     itemStyle: {
      //         normal: {
      //             borderColor: '#87cefa',
      //             borderWidth: 50,            // 标注边线线宽，单位px，默认为1
      //             label: {
      //                 show: true,
      //                 color: '#fff'
      //             }
      //         }
      //     },
      //  },
       },
      ],
    };

  }
}

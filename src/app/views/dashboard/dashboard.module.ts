import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { HttpClientModule } from '@angular/common/http';
import { InlineSVGModule } from 'ng-inline-svg';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CustomerCreditComponent } from '../customer-credit/customer-credit.component';
import { CreditConsultationComponent } from '../credit-consultation/credit-consultation.component';
import { ProductSoldComponent } from '../product-sold/product-sold.component';
import { NewBudgetComponent } from '../new-budget/new-budget.component';
const routes: Routes = [
  {
    path: 'home',
    component: DashboardComponent,
  },
  {
    path: 'customer-credit',
    component: CustomerCreditComponent,
  },
  {
    path: 'credit-consultation',
    component: CreditConsultationComponent,
  },
  {
    path: 'product-sold',
    component: ProductSoldComponent,
  },
  {
    path: 'new-budget',
    component: NewBudgetComponent,
  },

];

@NgModule({
  declarations: [DashboardComponent],
  imports: [
    NgbModule,
    CommonModule,
    RouterModule.forChild(routes),
    NgxEchartsModule,
    NgxDatatableModule,
    AngularSvgIconModule,
    HttpClientModule,
    InlineSVGModule,
    TooltipModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,


  ],
})
export class DashboardModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductSoldComponent } from './product-sold.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { InlineSVGModule } from 'ng-inline-svg';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { NgxEchartsModule } from 'ngx-echarts';

const routes: Routes = [
  {
    path: '',
    component: ProductSoldComponent,
  },
];

@NgModule({
  declarations: [ProductSoldComponent],
  imports: [
    NgbModule,
    CommonModule,
    RouterModule.forChild(routes),
    NgxEchartsModule,
    NgxDatatableModule,
    AngularSvgIconModule,
    HttpClientModule,
    InlineSVGModule,
    TooltipModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule
  ]
})
export class ProductSoldModule { }

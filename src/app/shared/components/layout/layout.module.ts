import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AuthLayoutComponent } from './auth-layout/auth-layout.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HeaderSidebarComponent } from './sidebar/header-sidebar/header-sidebar.component';
import { SidebarMenuComponent } from './sidebar/sidebar-menu/sidebar-menu.component';
import { SharedPipesModule } from '../../pipes/shared-pipes.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { HttpClientModule } from '@angular/common/http';
import { InlineSVGModule } from 'ng-inline-svg';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { NgxEchartsModule } from 'ngx-echarts';

const components = [
  AuthLayoutComponent,
  SidebarComponent,
  HeaderSidebarComponent,
  SidebarMenuComponent
];

@NgModule({
  declarations: [components],
  exports: [components],
  imports: [
    NgbModule,
    RouterModule,
    FormsModule,
    SharedPipesModule,
    CommonModule,
    PerfectScrollbarModule,
    AngularSvgIconModule,
    HttpClientModule,
    InlineSVGModule,
    NgxEchartsModule,
    NgxDatatableModule,
    TooltipModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,

  ],
})
export class LayoutModule {}

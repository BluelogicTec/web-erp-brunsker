import { Component, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableService } from 'src/app/shared/services/datatable-service.service';
import { NavigationService } from 'src/app/shared/services/navigation-service.service';

@Component({
  selector: 'app-header-sidebar',
  templateUrl: './header-sidebar.component.html',
  styleUrls: ['./header-sidebar.component.scss']
})
export class HeaderSidebarComponent implements OnInit {

  messages = {
    emptyMessage: `
      <div>
        <span class="classname">No results</span>
        <p>text</p>
      </div>
    `
  }
  // sessionStorageUtils = new SessionStorageUtils()
  userName: any;

  notifications: any[];
  closeResult = '';
  public rows: any;
  filterRadio = '1';
  message = 'Realize a busca acima para exibir os resultados'

  constructor(
    private navService: NavigationService,
    private modalService: NgbModal,
    private ngxDatatableService: NgxDatatableService,
  ) {

    this.notifications = [
      {
        icon: 'i-Speach-Bubble-6',
        title: 'Nova Mensagem',
        badge: '3',
        text: 'Teste 123465?',
        time: new Date(),
        status: 'primary',
        link: '/chat'
      },

    ];
  }

  ngOnInit(): void {
    this.rows = this.ngxDatatableService.modalSearch;
    // this.userName = this.sessionStorageUtils.obterUsuario()
  }

  // toggelSidebar() {
  //   const state = this.navService.sidebarState;
  //   if (state.childnavOpen && state.sidenavOpen) {
  //     return state.childnavOpen = false;
  //   }
  //   if (!state.childnavOpen && state.sidenavOpen) {
  //     return state.sidenavOpen = false;
  //   }
  //   // item has child items
  //   if (!state.sidenavOpen && !state.childnavOpen
  //     && this.navService.selectedItem.type === 'dropDown') {
  //       state.sidenavOpen = true;
  //       setTimeout(() => {
  //           state.childnavOpen = true;
  //       }, 50);
  //   }
  //   // item has no child items
  //   if (!state.sidenavOpen && !state.childnavOpen) {
  //     state.sidenavOpen = true;
  //   }
  // }

  logout() {
    // this.signinService.logout()
  }

  modalDate () {
    if(this.filterRadio == '1') {
      this.rows = this.ngxDatatableService.modalSearch;
    } else if (this.filterRadio == '2') {
      this.rows = this.ngxDatatableService.modalSearch2;
    }else if (this.filterRadio == '3') {
      this.rows = this.ngxDatatableService.modalSearch3;
    } else {
      this.rows = this.ngxDatatableService.modalSearch4;
    }

  }

  open(content: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      console.log(result)
      if(result === 'ok'){
      }
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }



}

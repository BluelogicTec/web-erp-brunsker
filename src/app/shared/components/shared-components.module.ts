import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { LayoutModule } from './layout/layout.module';
import { AngularSvgIconModule } from 'angular-svg-icon';

// const components = [
//   BtnLoadingComponent,
//   FeatherIconComponent,
// ];

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    LayoutModule,
    NgbModule
  ],
  declarations: [],
  exports: []
})
export class SharedComponentsModule { }

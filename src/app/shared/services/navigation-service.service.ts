import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export interface IMenuItem {
  id?: string;
  title?: string;
  description?: string;
  type: string; // Possible values: link/dropDown/extLink
  name?: string; // Used as display text for item and title for separator type
  state?: string; // Router state
  icon?: string; // Material icon name
  img?: string; // path img assets
  tooltip?: string; // Tooltip text
  disabled?: boolean; // If true, item will not be appeared in sidenav.
  sub?: IChildItem[]; // Dropdown items
  badges?: IBadge[];
  active?: boolean;

}
export interface IChildItem {
  id?: string;
  parentId?: string;
  type?: string;
  name: string; // Display text
  state?: string; // Router state
  icon?: string;
  sub?: IChildItem[];
  active?: boolean;
}

interface IBadge {
  color: string; // primary/accent/warn/hex color codes(#fff000)
  value: string; // Display text
}

interface ISidebarState {
  sidenavOpen?: boolean;
  childnavOpen?: boolean;
}

@Injectable({
  providedIn: 'root',
})
export class NavigationService {
  public sidebarState: ISidebarState = {
    sidenavOpen: true,
    childnavOpen: false,
  };
  selectedItem!: IMenuItem;

  constructor() {}

  defaultMenu: IMenuItem[] = [
    {
      name: 'Home',
      description: 'Controle',
      type: 'link',
      // icon: 'i-File-Clipboard-File--Text',
      img: './assets/icons/home.svg',
      state: '/dashboard/home',
    },
    {
      name: 'Novo orçamento',
      description: 'Formularios',
      type: 'link',
      img: './assets/icons/novo_orcamento.svg',
      state: '/dashboard/new-budget',
    },
    {
      name: 'Crédito cliente',
      description: 'Todos os pagamentos',
      type: 'link',
      img: './assets/icons/credito_cliente.svg',
      state: '/dashboard/customer-credit',
    },
    {
      name: 'Consultar orçamento',
      description: 'Todos os pagamentos',
      type: 'link',
      img: './assets/icons/consultar_orçamento.svg',
      state: '/dashboard/credit-consultation',
    },
    {
      name: 'Produto vendido',
      description: 'Todos os pagamentos',
      type: 'link',
      img: './assets/icons/produto_vendido.svg',
      state: '/dashboard/product-sold',
    },
    {
      name: 'Novo Cliente',
      description: 'Todos os pagamentos',
      type: 'link',
      img: './assets/icons/novo_cliente.svg',
      state: '/dashboard/payment',
    },
    {
      name: 'Deslogar',
      description: 'Todos os pagamentos',
      type: 'link',
      img: './assets/icons/deslogar.svg',
      state: '/auth/signin',
    },
  ];

  // sets iconMenu as default;
  menuItems = new BehaviorSubject<IMenuItem[]>(this.defaultMenu);
  // navigation component has subscribed to this Observable
  menuItems$ = this.menuItems.asObservable();

  // You can customize this method to supply different menu for
  // different user type.
  // publishNavigationChange(menuType: string) {
  //   switch (userType) {
  //     case 'admin':
  //       this.menuItems.next(this.adminMenu);
  //       break;
  //     case 'user':
  //       this.menuItems.next(this.userMenu);
  //       break;
  //     default:
  //       this.menuItems.next(this.defaultMenu);
  //   }
  // }
}

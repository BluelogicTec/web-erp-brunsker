import { Injectable } from '@angular/core';
import { from, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NgxDatatableService {
  hero_pages: any;
  total_count;
  historyClient: any;
  historyClientTotal_count;
  listCredit: any;
  listCredit_count;
  creditConsultationOrc: any;
  creditConsultationOrcTotal: any;
  creditConsultation: any;
  prductSold: any;
  modalSearch: any;
  modalSearch2: any;
  modalSearch3: any;
  modalSearch4: any;
  steep_item: any;
  steep_item_remove: any;
  constructor() {
    this.hero_pages = [
    {
      'id': '1',
      'cod': '89291',
      'orc': '1035634',
      'order': '19500220',
      'payment': 'Boleto',
      'rca': '195',
      'value': '536922',
      'status': 'Faturado',
      'action': '',
      'nameComp': 'ELETRON SPDA PROJETOS E SERVIÇO ...',
      'name': 'Pablo Bernardes Guimaraes'
    },
    {
      'id': '1',
      'cod': '89291',
      'orc': '1035634',
      'order': '19500220',
      'payment': 'Boleto',
      'rca': '195',
      'value': '536922',
      'status': 'Em autorização',
      'action': '',
      'nameComp': 'Teste ...',
      'name': 'Pablo'
    },
    {
      'id': '1',
      'cod': '89291',
      'orc': '1035634',
      'order': '19500220',
      'payment': 'Boleto',
      'rca': '195',
      'value': '536922',
      'status': 'Negado',
      'action': '',
      'nameComp': 'Teste ...',
      'name': 'Pablo'
    },
  ];

    this.total_count = this.hero_pages.length;

    this.listCredit = [
      {
        'id': '1',
        'date_lanc': '09/02/20 - 10:20:00',
        'lanc': '4567',
        'filial': '2',
        'type': 'C',
        'document': '98751',
        'typeCredit': 'DEPÓSITO',
        'typeClient': 'DEFINIDO',
        'codClient': '95693',
        'client': 'LUIZ OTAVIO',
        'matriz': '89',
      },
      {
        'id': '2',
        'date_lanc': '09/02/20 - 10:20:00',
        'lanc': '4567',
        'filial': '2',
        'type': 'C',
        'document': '984',
        'typeCredit': 'DEPÓSITO',
        'typeClient': 'DEFINIDO',
        'codClient': '95693',
        'client': 'MARCOS VELOSO MENOZES',
        'matriz': '891',
      },
      {
        'id': '2',
        'date_lanc': '09/02/20 - 10:20:00',
        'lanc': '4567',
        'filial': '2',
        'type': 'C',
        'document': '984',
        'typeCredit': 'DEPÓSITO',
        'typeClient': 'DEFINIDO',
        'codClient': '95693',
        'client': 'MARCOS VELOSO MENOZES',
        'matriz': '891',
      },
      {
        'id': '2',
        'date_lanc': '09/02/20 - 10:20:00',
        'lanc': '4567',
        'filial': '2',
        'type': 'C',
        'document': '984',
        'typeCredit': 'DEPÓSITO',
        'typeClient': 'DEFINIDO',
        'codClient': '95693',
        'client': 'MARCOS VELOSO MENOZES',
        'matriz': '891',
      },
      {
        'id': '2',
        'date_lanc': '09/02/20 - 10:20:00',
        'lanc': '4567',
        'filial': '2',
        'type': 'C',
        'document': '984',
        'typeCredit': 'DEPÓSITO',
        'typeClient': 'DEFINIDO',
        'codClient': '95693',
        'client': 'MARCOS VELOSO MENOZES',
        'matriz': '891',
      },
      {
        'id': '2',
        'date_lanc': '09/02/20 - 10:20:00',
        'lanc': '4567',
        'filial': '2',
        'type': 'C',
        'document': '984',
        'typeCredit': 'DEPÓSITO',
        'typeClient': 'DEFINIDO',
        'codClient': '95693',
        'client': 'MARCOS VELOSO MENOZES',
        'matriz': '891',
      },
      {
        'id': '2',
        'date_lanc': '09/02/20 - 10:20:00',
        'lanc': '4567',
        'filial': '2',
        'type': 'C',
        'document': '984',
        'typeCredit': 'DEPÓSITO',
        'typeClient': 'DEFINIDO',
        'codClient': '95693',
        'client': 'MARCOS VELOSO MENOZES',
        'matriz': '891',
      }
    ]

    this.listCredit_count = this.listCredit.length;

    this.historyClient = [
      {
        'id': '1',
        'date_lanc': '09/02/2021 - 10:22:43',
        'lanc': '4567',
        'filial': '2',
        'type': 'C',
        'document': '98546',
        'typeCredit': 'DEPÓSITO',
        'typeClient': 'DEFINIDO',
        'codClient': '956963',
        'client': 'LUIS OTAVIO KALIL',
        'matriz': '9895',
      },
      {
        'id': '1',
        'date_lanc': '09/02/2021 - 10:22:43',
        'lanc': '4567',
        'filial': '2',
        'type': 'C',
        'document': '98546',
        'typeCredit': 'DEPÓSITO',
        'typeClient': 'DEFINIDO',
        'codClient': '956963',
        'client': 'LUIS OTAVIO KALIL',
        'matriz': '9895',
      }
    ]

    this.historyClientTotal_count = this.listCredit.length;

    this.creditConsultation = [
      {
        'id': '1',
        'orc_data': '09/02/2021',
        'orc_filial': '1053505',
        'filial': '2',
        'noteN': '1053505',
        'dateE': '09/02/2021',
        'codClient': '52654654',
        'client': 'GM CONSTRUTORA EIRELI',
        'total': 'R$ 25.00',
        'status': 'EM ABERTO',
        'matriz': '1231657498',
      },
      {
        'id': '1',
        'orc_data': '09/02/2021',
        'orc_filial': '1053505',
        'filial': '2',
        'noteN': '1053505',
        'dateE': '09/02/2021',
        'codClient': '52654654',
        'client': 'GM CONSTRUTORA EIRELI',
        'total': 'R$ 25.00',
        'status': 'EM ABERTO',
        'matriz': '1231657498',
      },
      {
        'id': '1',
        'orc_data': '09/02/2021',
        'orc_filial': '1053505',
        'filial': '2',
        'noteN': '1053505',
        'dateE': '09/02/2021',
        'codClient': '52654654',
        'client': 'GM CONSTRUTORA EIRELI',
        'total': 'R$ 25.00',
        'status': 'EM ABERTO',
        'matriz': '1231657498',
      },
      {
        'id': '1',
        'orc_data': '09/02/2021',
        'orc_filial': '1053505',
        'filial': '2',
        'noteN': '1053505',
        'dateE': '09/02/2021',
        'codClient': '52654654',
        'client': 'GM CONSTRUTORA EIRELI',
        'total': 'R$ 25.00',
        'status': 'EM ABERTO',
        'matriz': '1231657498',
      },
      {
        'id': '1',
        'orc_data': '09/02/2021',
        'orc_filial': '1053505',
        'filial': '2',
        'noteN': '1053505',
        'dateE': '09/02/2021',
        'codClient': '52654654',
        'client': 'GM CONSTRUTORA EIRELI',
        'total': 'R$ 25.00',
        'status': 'EM ABERTO',
        'matriz': '1231657498',
      },{
        'id': '1',
        'orc_data': '09/02/2021',
        'orc_filial': '1053505',
        'filial': '2',
        'noteN': '1053505',
        'dateE': '09/02/2021',
        'codClient': '52654654',
        'client': 'GM CONSTRUTORA EIRELI',
        'total': 'R$ 25.00',
        'status': 'EM ABERTO',
        'matriz': '1231657498',
      },
    ]


    this.creditConsultationOrc = [
      {
        'id': '1',
        'item': '5',
        'cod': '86495',
        'description': 'PLACA 4X2 CEGA LIZ BRANCA C/SUPORTE',
        'ncm': '39259090',
        'unid': 'PC',
        'ref': '39259090',
        'emb': 'TRAMONTINA',
        'pcoTable': 'R$ 34,46',
        'discount': '5%',
        'pcoLiquid': 'EM ABERTO',
        'matriz': '1231657498',
      },
      {
        'id': '1',
        'item': '5',
        'cod': '86495',
        'description': 'PLACA 4X2 CEGA LIZ BRANCA C/SUPORTE',
        'ncm': '39259090',
        'unid': 'PC',
        'ref': '39259090',
        'emb': 'TRAMONTINA',
        'pcoTable': 'R$ 34,46',
        'discount': '5%',
        'pcoLiquid': 'EM ABERTO',
        'matriz': '1231657498',
      },{
        'id': '1',
        'item': '5',
        'cod': '86495',
        'description': 'PLACA 4X2 CEGA LIZ BRANCA C/SUPORTE',
        'ncm': '39259090',
        'unid': 'PC',
        'ref': '39259090',
        'emb': 'TRAMONTINA',
        'pcoTable': 'R$ 34,46',
        'discount': '5%',
        'pcoLiquid': 'EM ABERTO',
        'matriz': '1231657498',
      },{
        'id': '1',
        'item': '5',
        'cod': '86495',
        'description': 'PLACA 4X2 CEGA LIZ BRANCA C/SUPORTE',
        'ncm': '39259090',
        'unid': 'PC',
        'ref': '39259090',
        'emb': 'TRAMONTINA',
        'pcoTable': 'R$ 34,46',
        'discount': '5%',
        'pcoLiquid': 'EM ABERTO',
        'matriz': '1231657498',
      },{
        'id': '1',
        'item': '5',
        'cod': '86495',
        'description': 'PLACA 4X2 CEGA LIZ BRANCA C/SUPORTE',
        'ncm': '39259090',
        'unid': 'PC',
        'ref': '39259090',
        'emb': 'TRAMONTINA',
        'pcoTable': 'R$ 34,46',
        'discount': '5%',
        'pcoLiquid': 'EM ABERTO',
        'matriz': '1231657498',
      },
    ]

    this.prductSold = [
      {
        'id': '1',
        'date': '09/02/2021 - 10:22:43',
        'noteN': '1053505',
        'orc_filial': '1053505',
        'codClient': '52654654',
        'client': 'SPE SAUDE PRIMARIA BH S.A.',
        'rca': '19',
        'codProduct': '5515',
        'product': 'R$ 25.00'
      },
      {
        'id': '1',
        'date': '09/02/2021 - 10:22:43',
        'noteN': '1053505',
        'orc_filial': '1053505',
        'codClient': '52654654',
        'client': 'SPE SAUDE PRIMARIA BH S.A.',
        'rca': '19',
        'codProduct': '5515',
        'product': 'R$ 25.00'
      },
      {
        'id': '1',
        'date': '09/02/2021 - 10:22:43',
        'noteN': '1053505',
        'orc_filial': '1053505',
        'codClient': '52654654',
        'client': 'SPE SAUDE PRIMARIA BH S.A.',
        'rca': '19',
        'codProduct': '5515',
        'product': 'R$ 25.00'
      },
      {
        'id': '1',
        'date': '09/02/2021 - 10:22:43',
        'noteN': '1053505',
        'orc_filial': '1053505',
        'codClient': '52654654',
        'client': 'SPE SAUDE PRIMARIA BH S.A.',
        'rca': '19',
        'codProduct': '5515',
        'product': 'R$ 25.00'
      },
      {
        'id': '1',
        'date': '09/02/2021 - 10:22:43',
        'noteN': '1053505',
        'orc_filial': '1053505',
        'codClient': '52654654',
        'client': 'SPE SAUDE PRIMARIA BH S.A.',
        'rca': '19',
        'codProduct': '5515',
        'product': 'R$ 25.00'
      },
      {
        'id': '1',
        'date': '09/02/2021 - 10:22:43',
        'noteN': '1053505',
        'orc_filial': '1053505',
        'codClient': '52654654',
        'client': 'SPE SAUDE PRIMARIA BH S.A.',
        'rca': '19',
        'codProduct': '5515',
        'product': 'R$ 25.00'
      }
    ]

    this.modalSearch = [
      {
        'id': '1',
        'customerId': '2154',
        'companyName': 'CONARTES ENGENHARIA E EDIFICACOES',
        'fantasy': 'CLAUDIA CONSTANTINA',
        'cnpj': '40.227.347/0001-01',
        'rca': '16',
        'primaryCustomer': 'CLAUDIA CONSTANTINA'
      },
      {
        'id': '2',
        'customerId': '2154',
        'companyName': 'CONARTES ENGENHARIA E EDIFICACOES',
        'fantasy': 'CLAUDIA CONSTANTINA',
        'cnpj': '40.227.347/0001-01',
        'rca': '16',
        'primaryCustomer': 'CLAUDIA CONSTANTINA'
      },
      {
        'id': '3',
        'customerId': '2154',
        'companyName': 'CONARTES ENGENHARIA E EDIFICACOES',
        'fantasy': 'CLAUDIA CONSTANTINA',
        'cnpj': '40.227.347/0001-01',
        'rca': '16',
        'primaryCustomer': 'CLAUDIA CONSTANTINA'
      },
      {
        'id': '4',
        'customerId': '2154',
        'companyName': 'CONARTES ENGENHARIA E EDIFICACOES',
        'fantasy': 'CLAUDIA CONSTANTINA',
        'cnpj': '40.227.347/0001-01',
        'rca': '16',
        'primaryCustomer': 'CLAUDIA CONSTANTINA'
      },
      {
        'id': '5',
        'customerId': '2154',
        'companyName': 'CONARTES ENGENHARIA E EDIFICACOES',
        'fantasy': 'CLAUDIA CONSTANTINA',
        'cnpj': '40.227.347/0001-01',
        'rca': '16',
        'primaryCustomer': 'CLAUDIA CONSTANTINA'
      },
      {
        'id': '6',
        'customerId': '2154',
        'companyName': 'CONARTES ENGENHARIA E EDIFICACOES',
        'fantasy': 'CLAUDIA CONSTANTINA',
        'cnpj': '40.227.347/0001-01',
        'rca': '16',
        'primaryCustomer': 'CLAUDIA CONSTANTINA'
      },
      {
        'id': '7',
        'customerId': '2154',
        'companyName': 'CONARTES ENGENHARIA E EDIFICACOES',
        'fantasy': 'CLAUDIA CONSTANTINA',
        'cnpj': '40.227.347/0001-01',
        'rca': '16',
        'primaryCustomer': 'CLAUDIA CONSTANTINA'
      },
      {
        'id': '8',
        'customerId': '2154',
        'companyName': 'CONARTES ENGENHARIA E EDIFICACOES',
        'fantasy': 'CLAUDIA CONSTANTINA',
        'cnpj': '40.227.347/0001-01',
        'rca': '16',
        'primaryCustomer': 'CLAUDIA CONSTANTINA'
      }
    ]

    this.modalSearch2 = [
    ]
    this.modalSearch3 = [
    ]
    this.modalSearch4 = [
    ]


    this.steep_item = [
      {
        'id': '1',
        'qtde': '',
        'cod': '86495',
        'desc_product': 'PLACA 4X2 CEGA LIZ BRANCA C/SUPORTE',
        'ncm': '39259090',
        'unid': 'PC',
        'ref_fab': '57102095',
        'emb': 'TRAMONTINA',
        'pc_table': 'R$ 34,46',
        'dec': '5%',
        'pc_liquid': '5%'
      }, 
      {
        'id': '2',
        'qtde': '',
        'cod': '86495',
        'desc_product': 'PLACA 4X2 CEGA LIZ BRANCA C/SUPORTE',
        'ncm': '39259090',
        'unid': 'PC',
        'ref_fab': '57102095',
        'emb': 'TRAMONTINA',
        'pc_table': 'R$ 34,46',
        'dec': '5%',
        'pc_liquid': '5%'
      }, 
      {
        'id': '3',
        'qtde': '5',
        'cod': '86495',
        'desc_product': 'PLACA 4X2 CEGA LIZ BRANCA C/SUPORTE',
        'ncm': '39259090',
        'unid': 'PC',
        'ref_fab': '57102095',
        'emb': 'TRAMONTINA',
        'pc_table': 'R$ 34,46',
        'dec': '5%',
        'pc_liquid': '5%'
      }, 
      {
        'id': '4',
        'qtde': '',
        'cod': '86495',
        'desc_product': 'PLACA 4X2 CEGA LIZ BRANCA C/SUPORTE',
        'ncm': '39259090',
        'unid': 'PC',
        'ref_fab': '57102095',
        'emb': 'TRAMONTINA',
        'pc_table': 'R$ 34,46',
        'dec': '5%',
        'pc_liquid': '5%'
      }, 
      {
        'id': '5',
        'qtde': '',
        'cod': '86495',
        'desc_product': 'PLACA 4X2 CEGA LIZ BRANCA C/SUPORTE',
        'ncm': '39259090',
        'unid': 'PC',
        'ref_fab': '57102095',
        'emb': 'TRAMONTINA',
        'pc_table': 'R$ 34,46',
        'dec': '5%',
        'pc_liquid': '5%'
      }, 
      {
        'id': '6',
        'qtde': '4',
        'cod': '86495',
        'desc_product': 'PLACA 4X2 CEGA LIZ BRANCA C/SUPORTE',
        'ncm': '39259090',
        'unid': 'PC',
        'ref_fab': '57102095',
        'emb': 'TRAMONTINA',
        'pc_table': 'R$ 34,46',
        'dec': '5%',
        'pc_liquid': '5%'
      }, 
      {
        'id': '7',
        'qtde': '8',
        'cod': '86495',
        'desc_product': 'PLACA 4X2 CEGA LIZ BRANCA C/SUPORTE',
        'ncm': '39259090',
        'unid': 'PC',
        'ref_fab': '57102095',
        'emb': 'TRAMONTINA',
        'pc_table': 'R$ 34,46',
        'dec': '5%',
        'pc_liquid': '5%'
      },
    ]

    this.steep_item_remove = [
      {
        'id': '3',
        'qtde': '5',
        'cod': '86495',
        'desc_product': 'PLACA 4X2 CEGA LIZ BRANCA C/SUPORTE',
        'ncm': '39259090',
        'unid': 'PC',
        'ref_fab': '57102095',
        'emb': 'TRAMONTINA',
        'pc_table': 'R$ 34,46',
        'dec': '5%',
        'pc_liquid': '5%'
      },  
      {
        'id': '6',
        'qtde': '4',
        'cod': '86495',
        'desc_product': 'PLACA 4X2 CEGA LIZ BRANCA C/SUPORTE',
        'ncm': '39259090',
        'unid': 'PC',
        'ref_fab': '57102095',
        'emb': 'TRAMONTINA',
        'pc_table': 'R$ 34,46',
        'dec': '5%',
        'pc_liquid': '5%'
      }, 
      {
        'id': '7',
        'qtde': '8',
        'cod': '86495',
        'desc_product': 'PLACA 4X2 CEGA LIZ BRANCA C/SUPORTE',
        'ncm': '39259090',
        'unid': 'PC',
        'ref_fab': '57102095',
        'emb': 'TRAMONTINA',
        'pc_table': 'R$ 34,46',
        'dec': '5%',
        'pc_liquid': '5%'
      },
    ]
  }
}

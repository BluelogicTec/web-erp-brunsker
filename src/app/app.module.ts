import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {MatButtonModule} from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedPipesModule } from './shared/pipes/shared-pipes.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { DashboardModule } from './views/dashboard/dashboard.module';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import * as echarts from 'echarts/core';
import { LineChart } from 'echarts/charts';

import {
  TitleComponent,
  TooltipComponent,
  GridComponent
} from 'echarts/components';

import {
  CanvasRenderer
} from 'echarts/renderers';
import 'echarts/theme/macarons.js';
import { CustomerCreditModule } from './views/customer-credit/customer-credit.module';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { MatIconModule } from '@angular/material/icon';
import { HttpClientModule } from '@angular/common/http';
import { InlineSVGModule } from 'ng-inline-svg';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { ptBrLocale } from 'ngx-bootstrap/locale';
import { CreditConsultationModule } from './views/credit-consultation/credit-consultation.module';
import { ProductSoldModule } from './views/product-sold/product-sold.module';
import { NewBudgetModule } from './views/new-budget/new-budget.module';

defineLocale('pt-br', ptBrLocale);

echarts.use(
  [TitleComponent, TooltipComponent, GridComponent, LineChart, CanvasRenderer]
);

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    ReactiveFormsModule,
    FormsModule,
    SharedPipesModule,
    NgbModule,
    PerfectScrollbarModule,
    DashboardModule,
    NewBudgetModule,
    NgxEchartsModule,
    NgxDatatableModule,
    CustomerCreditModule,
    CreditConsultationModule,
    ProductSoldModule,
    HttpClientModule,
    InlineSVGModule,
    AngularSvgIconModule.forRoot(),
    BsDatepickerModule.forRoot(),
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
